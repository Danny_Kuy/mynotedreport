package com.danny.mynotedreport.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import static com.danny.mynotedreport.utils.Constants.APP_SHARED_PREFS;

/**
 * Created by Danny on 3/23/2018.
 */

public class MySharePreference {

    private static MySharePreference instance = null;
    private SharedPreferences mSharedPreferences;

    public MySharePreference(Context context) {
        mSharedPreferences = context.getSharedPreferences(APP_SHARED_PREFS, Activity.MODE_PRIVATE);
    }

    public static MySharePreference with(Context context) {
        if (instance == null) {
            instance = new MySharePreference(context);
        }
        return instance;
    }

    public void clearSharedPreference() {
        mSharedPreferences.edit().clear().apply();
    }

    public void setLogin(boolean isLogin) {
        mSharedPreferences.edit().putBoolean(Constants.IS_LOGIN, isLogin).apply();
    }

    public boolean isLogin() {
        return mSharedPreferences.getBoolean(Constants.IS_LOGIN, false);
    }
}
