package com.danny.mynotedreport.utils;

/**
 * Created by Danny on 3/21/2018.
 */

public class Constants {

    public static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    public static final String APP_SHARED_PREFS = "MyNotedReport";
    public static final String IS_LOGIN = "IS_LOGIN";

    public class RequestCode {
        public static final int G_PLUS = 1680;
    }
}
