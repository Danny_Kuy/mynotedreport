package com.danny.mynotedreport.customs;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;

import com.danny.mynotedreport.R;

/**
 * Created by Danny on 3/23/2018.
 */

public class ErrorView extends FrameLayout {

    enum ErrorMode {
        LOADING,
        NO_DATA
    }

    public ErrorView(@NonNull Context context) {
        super(context);
        initView();
    }

    public ErrorView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public ErrorView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    private void initView() {
        setVisibility(GONE);
        LayoutInflater.from(getContext()).inflate(R.layout.view_error, this);
    }

    public void setErrorMode(ErrorMode errorMode) {
        setVisibility(VISIBLE);
        switch (errorMode) {
            case NO_DATA:
                findViewById(R.id.layout_no_data).setVisibility(VISIBLE);
                findViewById(R.id.progress_bar).setVisibility(GONE);
                break;
            case LOADING:
                findViewById(R.id.layout_no_data).setVisibility(GONE);
                findViewById(R.id.progress_bar).setVisibility(VISIBLE);
                break;
            default:
        }
    }
}
