package com.danny.mynotedreport.customs;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.danny.mynotedreport.R;

/**
 * Created by Danny on 3/23/2018.
 */

public class CustomToolbarView extends FrameLayout {
    private TypedArray typedArray;
    private OnToolbarIconListener onToolbarIconListener;

    public CustomToolbarView(Context context) {
        super(context);
    }

    public CustomToolbarView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView(attrs);
    }

    public CustomToolbarView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(attrs);
    }

    public void setToolbarActionIcon(int... drawableId) {
        LinearLayout layoutAction = findViewById(R.id.toolbar_action_container);
        for (int i = 0; i < drawableId.length; i++) {
            ImageView imageView = new ImageView(getContext());
            imageView.setImageResource(drawableId[i]);
            layoutAction.addView(imageView);
            final int finalI = i;
            imageView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    onToolbarIconListener.onActionClick(finalI);
                }
            });
        }
    }

    public void setOnToolbarIconListener(OnToolbarIconListener onToolbarIconListener) {
        this.onToolbarIconListener = onToolbarIconListener;
    }

    public void setTitle(String title) {
        ((TextView) findViewById(R.id.toolbar_title)).setText(title);
    }

    private void initView(AttributeSet attrs) {
        LayoutInflater.from(getContext()).inflate(R.layout.view_custom_toolbar, this);
        typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.CustomToolbarView);
        if (typedArray != null) {
            setTitle(typedArray.getString(R.styleable.CustomToolbarView_toolbarTitle));
            ((ImageView) findViewById(R.id.toolbar_navigate)).setImageResource((typedArray.getResourceId(R.styleable.CustomToolbarView_toolbarNavigate, R.drawable.ic_menu_white_24dp)));
            findViewById(R.id.main).setBackgroundColor(typedArray.getColor(R.styleable.CustomToolbarView_toolbarColor, getResources().getColor(R.color.colorPrimary)));
        }
        typedArray.recycle();
        findViewById(R.id.toolbar_navigate).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onToolbarIconListener != null)
                    onToolbarIconListener.onNavigateClick();
            }
        });
    }

    public interface OnToolbarIconListener {
        void onNavigateClick();

        void onActionClick(int index);
    }
}
