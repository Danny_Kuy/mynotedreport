package com.danny.mynotedreport.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.danny.mynotedreport.R;
import com.danny.mynotedreport.activities.NoteDetailActivity;
import com.google.firebase.database.DataSnapshot;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Danny on 3/23/2018.
 */

public class NoteAdapter extends RecyclerView.Adapter {

    private List<DataSnapshot> mDataSnapshots = new ArrayList<>();

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new NoteViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.holder_note_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ((NoteViewHolder) holder).onBind();
    }

    @Override
    public int getItemCount() {
        return mDataSnapshots.size();
    }

    public void addAllItems(List<DataSnapshot> dataSnapshots) {
        mDataSnapshots.clear();
        mDataSnapshots.addAll(dataSnapshots);
        notifyDataSetChanged();
    }

    private class NoteViewHolder extends RecyclerView.ViewHolder {
        TextView textViewText;

        public NoteViewHolder(View itemView) {
            super(itemView);
            textViewText = itemView.findViewById(R.id.text);
        }

        void onBind() {
            textViewText.setText(mDataSnapshots.get(getAdapterPosition()).child("text").getValue(String.class));
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    NoteDetailActivity.launch(v.getContext(), mDataSnapshots.get(getAdapterPosition()).getKey());
                }
            });
        }
    }
}
