package com.danny.mynotedreport.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.test.espresso.core.internal.deps.guava.collect.Lists;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.danny.mynotedreport.R;
import com.danny.mynotedreport.bases.BaseActivity;
import com.danny.mynotedreport.customs.CustomToolbarView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

/**
 * Created by Danny on 3/23/2018.
 */

public class NoteDetailActivity extends BaseActivity implements CustomToolbarView.OnToolbarIconListener {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_detail);

        CustomToolbarView customToolbarView = findViewById(R.id.custom_toolbar_view);
        customToolbarView.setTitle("Detail");
        customToolbarView.setOnToolbarIconListener(this);

        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = database.getReference().child(getCurrentUser().getUid()).child("note/" + getIntent().getStringExtra("KEY"));
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.d("Note Data", dataSnapshot.toString());
                LinearLayout linearLayout = findViewById(R.id.layout_note);
                if ((boolean) dataSnapshot.child("is_check_list").getValue()) {
                    ArrayList<DataSnapshot> dataCheckList = Lists.newArrayList(dataSnapshot.child("data").getChildren());
                    CheckBox checkBox;
                    for (DataSnapshot checkItem : dataCheckList) {
                        checkBox = new CheckBox(NoteDetailActivity.this);
                        checkBox.setText(checkItem.child("text").getValue(String.class));
                        checkBox.setTextSize(20);
                        checkBox.setChecked((boolean) checkItem.child("is_check").getValue());
                        if (checkBox.isChecked())
                            checkBox.setPaintFlags(checkBox.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                        linearLayout.addView(checkBox);
                    }
                } else {
                    EditText editText = new EditText(NoteDetailActivity.this);
                    editText.setBackground(null);
                    editText.setText(dataSnapshot.child("text").getValue(String.class));
                    editText.setTextColor(ContextCompat.getColor(NoteDetailActivity.this, android.R.color.black));
                    editText.setTextSize(20);
                    editText.clearFocus();
                    linearLayout.addView(editText);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d("EDDDDDD", databaseError.toString());
            }
        });
    }

    @Override
    public void onNavigateClick() {
        onBackPressed();
    }

    @Override
    public void onActionClick(int index) {

    }

    public static void launch(Context context, String key) {
        context.startActivity(new Intent(context, NoteDetailActivity.class).putExtra("KEY", key));
    }
}
