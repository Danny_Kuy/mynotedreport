package com.danny.mynotedreport.activities;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;

import com.danny.mynotedreport.utils.MySharePreference;
import com.danny.mynotedreport.R;
import com.danny.mynotedreport.bases.BaseActivity;

/**
 * Created by Danny on 3/23/2018.
 */

public class SplashScreenActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        setStatusBarTransparent();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (MySharePreference.with(SplashScreenActivity.this).isLogin()) {
                    MainActivity.launch(SplashScreenActivity.this);
                } else {
                    SignInActivity.launch(SplashScreenActivity.this);
                }
            }
        }, 3000);
    }
}
