package com.danny.mynotedreport.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.danny.mynotedreport.R;
import com.danny.mynotedreport.bases.BaseActivity;
import com.danny.mynotedreport.customs.CustomDialog;
import com.danny.mynotedreport.utils.Constants;
import com.danny.mynotedreport.utils.MySharePreference;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.GoogleAuthProvider;

/**
 * Created by Danny on 3/21/2018.
 */

public class SignInActivity extends BaseActivity implements View.OnClickListener {

    private CustomDialog customDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        setStatusBarTransparent();
        findViewById(R.id.layout_sign_up).setOnClickListener(this);
        findViewById(R.id.button_sign_in).setOnClickListener(this);
        findViewById(R.id.button_sign_in_google).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.layout_sign_up:
                showSignUpDialog();
                break;
            case R.id.button_sign_in:
                onSignIn();
                break;
            case R.id.button_sign_in_google:
                onSignInGoogle();
                break;
            default:
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.RequestCode.G_PLUS) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()) {
                GoogleSignInAccount account = result.getSignInAccount();
                firebaseAuthWithGoogle(account);
            } else {
                Toast.makeText(this, "Sign In Failed.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());
        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "signInWithCredential:onComplete:" + task.isSuccessful());

                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
                            signOut();
                            Log.w(TAG, "signInWithCredential", task.getException());
                        } else {
                            onSignInSuccess();
                        }
                    }
                });
    }

    private void onSignInGoogle() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        GoogleApiClient mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                        Log.d(TAG, "onConnectionFailed:" + connectionResult);
                        Toast.makeText(SignInActivity.this, "Google Play Services error.", Toast.LENGTH_SHORT).show();
                    }
                })
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, Constants.RequestCode.G_PLUS);
    }

    private void onSignIn() {
        EditText editTextEmail = findViewById(R.id.edit_text_sign_in_email);
        EditText editTextPassword = findViewById(R.id.edit_text_sign_in_password);
        String email = editTextEmail.getText().toString().trim();
        String password = editTextPassword.getText().toString().trim();
        if (TextUtils.isEmpty(email)) {
            editTextEmail.setError("What is your email?");
            return;
        }
        if (TextUtils.isEmpty(password)) {
            editTextEmail.setError("What is your password?");
            return;
        }
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "signInWithEmail:success");
                            onSignInSuccess();
                        } else {
                            Log.w(TAG, "signInWithEmail:failure", task.getException());
                            Toast.makeText(SignInActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void onSignInSuccess() {
        MySharePreference.with(SignInActivity.this).setLogin(true);
        MainActivity.launch(SignInActivity.this);
    }

    private void showSignUpDialog() {
        View view = LayoutInflater.from(this).inflate(R.layout.dialog_sign_up, null);
        final EditText editTextEmail = view.findViewById(R.id.edit_text_email);
        final EditText editTextPassword = view.findViewById(R.id.edit_text_password);
        final EditText editTextConfirmPassword = view.findViewById(R.id.edit_text_confirm_password);
        view.findViewById(R.id.btn_sign_up).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = editTextEmail.getText().toString().trim();
                String password = editTextPassword.getText().toString().trim();
                String confirmPassword = editTextConfirmPassword.getText().toString().trim();
                if (TextUtils.isEmpty(email)) {
                    editTextEmail.setError("What is your email?");
                    return;
                }
                if (TextUtils.isEmpty(password)) {
                    editTextPassword.setError("What is your password?");
                    return;
                }
                if (TextUtils.isEmpty(confirmPassword)) {
                    editTextConfirmPassword.setError("What is your confirm password?");
                    return;
                }
                if (!TextUtils.equals(password, confirmPassword)) {
                    editTextPassword.setError("Password not match.");
                    editTextConfirmPassword.setError("Confirm password not match.");
                    return;
                }
                onSignUp(email, password);
            }
        });
        customDialog = new CustomDialog(this);
        customDialog.setContentView(view);
        customDialog.show();
    }

    private void onSignUp(String email, String password) {
        final ProgressDialog progressDialog = ProgressDialog.show(this, "", "Signing up...");
        progressDialog.show();
        mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(this,
                new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        progressDialog.dismiss();
                        customDialog.dismiss();
                        if (task.isSuccessful()) {
                            Log.d(TAG, "createUserWithEmail:success");
                            Toast.makeText(SignInActivity.this, "Success Sign Up.", Toast.LENGTH_SHORT).show();
                        } else {
                            Log.w(TAG, "createUserWithEmail:failure", task.getException());
                            Toast.makeText(SignInActivity.this, "Failed Sign Up : " + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    public static void launch(Context context) {
        context.startActivity(new Intent(context, SignInActivity.class));
    }
}
