package com.danny.mynotedreport.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.danny.mynotedreport.R;
import com.danny.mynotedreport.bases.BaseActivity;
import com.danny.mynotedreport.customs.CustomToolbarView;


public class MainActivity extends BaseActivity implements CustomToolbarView.OnToolbarIconListener {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        CustomToolbarView customToolbarView = findViewById(R.id.custom_toolbar_view);
        customToolbarView.setOnToolbarIconListener(this);
        customToolbarView.setToolbarActionIcon(R.drawable.ic_exit_to_app_white_24dp);
    }

    public void onReportClick(View view) {
        DailyReportActivity.launch(this);
    }

    public void onNoteClick(View view) {
        NoteActivity.launch(this);
    }

    public static void launch(Context context) {
        context.startActivity(new Intent(context, MainActivity.class));
    }

    @Override
    public void onNavigateClick() {

    }

    @Override
    public void onActionClick(int index) {
        if (index == 0) {
            signOut();
            Intent intent = new Intent(this, SignInActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
    }
}
