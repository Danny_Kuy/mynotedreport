package com.danny.mynotedreport.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.test.espresso.core.internal.deps.guava.collect.Lists;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.danny.mynotedreport.R;
import com.danny.mynotedreport.adapters.NoteAdapter;
import com.danny.mynotedreport.bases.BaseActivity;
import com.danny.mynotedreport.customs.CustomToolbarView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

/**
 * Created by Danny on 3/21/2018.
 */

public class NoteActivity extends BaseActivity implements CustomToolbarView.OnToolbarIconListener {

    private NoteAdapter noteAdapter;

    private View.OnClickListener addNote = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

        }
    };

    public static void launch(Context context) {
        context.startActivity(new Intent(context, NoteActivity.class));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note);
        ((CustomToolbarView) findViewById(R.id.custom_toolbar_view)).setOnToolbarIconListener(this);

        RecyclerView recyclerView = findViewById(R.id.recycler_view_note);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        noteAdapter = new NoteAdapter();
        recyclerView.setAdapter(noteAdapter);

        findViewById(R.id.btn_add_note).setOnClickListener(addNote);

        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = database.getReference().child(getCurrentUser().getUid()).child("note");
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.d("EDDDDDD", dataSnapshot.toString());
                noteAdapter.addAllItems(Lists.newArrayList(dataSnapshot.getChildren()));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d("EDDDDDD", databaseError.toString());
            }
        });
    }

    @Override
    public void onNavigateClick() {
        onBackPressed();
    }

    @Override
    public void onActionClick(int index) {

    }
}
