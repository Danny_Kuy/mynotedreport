package com.danny.mynotedreport.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.danny.mynotedreport.R;
import com.danny.mynotedreport.bases.BaseActivity;

/**
 * Created by Danny on 3/21/2018.
 */

public class DailyReportActivity extends BaseActivity {

    public static void launch(Context context) {
        context.startActivity(new Intent(context, DailyReportActivity.class));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);

        RecyclerView recyclerView = findViewById(R.id.recycler_view_report);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(new ReportAdapter());
    }

    private class ReportAdapter extends RecyclerView.Adapter {
        @NonNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new ReportViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.holder_report_item, parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
            ((ReportViewHolder) holder).onBind();
        }

        @Override
        public int getItemCount() {
            findViewById(R.id.layout_no_data).setVisibility(getItemCount() == 0 ? View.VISIBLE : View.GONE);
            return 0;
        }
    }

    private class ReportViewHolder extends RecyclerView.ViewHolder {

        ReportViewHolder(View itemView) {
            super(itemView);
        }

        void onBind() {

        }
    }
}
